#!/usr/bin/env python3


import os
import sys
import subprocess as sp
import json
import paramiko
import xml.etree.ElementTree as ET
import serial
import io

def run_and_getout(command):
    fd = sp.Popen(command, shell=True, stdout=sp.PIPE)
    return fd.communicate()[0]

class Tools(object):
    def __init__(self):
        self.default_code = sys.getdefaultencoding()
        pass

    def xml_get_name(self, xml_file):
        """
        <domain type='kvm'>
        <name>guest30032</name>
        <uuid>37425e76-af6a-44a6-aba0-73434afe34c0</uuid>
        </domain>
        """
        tree = ET.parse(xml_file)
        root = tree.getroot()
        name_item = ET.ElementPath.find(root, "./name")
        if name_item != None:
            return name_item.text
        else:
            return ""

    def xml_get_uuid_from_xml_file(self, xml_file):
        """
        <domain type='kvm'>
        <name>guest30032</name>
        <uuid>37425e76-af6a-44a6-aba0-73434afe34c0</uuid>
        </domain>
        """
        tree = ET.parse(xml_file)
        root = tree.getroot()
        uuid_item = ET.ElementPath.find(root, "./uuid")
        if uuid_item != None:
            return uuid_item.text
        else:
            return ""

    def xml_update_guestname_and_uuid(self, xml_file, name, uuid):
        """
        <domain type='kvm'>
        <name>guest30032</name>
        <uuid>37425e76-af6a-44a6-aba0-73434afe34c0</uuid>
        </domain>
        """
        tree = ET.parse(xml_file)
        root = tree.getroot()
        name_item = ET.ElementPath.find(root, "./name")
        uuid_item = ET.ElementPath.find(root, "./uuid")
        if name_item is not None and name is not None:
            name_item.text = str(name)
        if uuid_item is not None and uuid is not None:
            uuid_item.text = str(uuid)
        tree.write(xml_file)

    def xml_add_vcpupin_item(self, xml_file, num):
        """
        <vcpu placement="static">3</vcpu>
        <cputune>
            <vcpupin cpuset="1" vcpu="0" />
            <vcpupin cpuset="2" vcpu="1" />
            <vcpupin cpuset="3" vcpu="2" />
        </cputune>
        """
        tree = ET.parse(xml_file)
        root = tree.getroot()
        vcpu_item = ET.ElementPath.find(root, "./vcpu")
        if None != vcpu_item:
            vcpu_item.text = str(num)
        tree.write(xml_file)
        self.remove_item_from_xml(xml_file,"./cputune/vcpupin")
        self.remove_item_from_xml(xml_file,"./cputune/emulatorpin")
        for i in range(num):
            temp_info = f"""
            <vcpupin vcpu="{str(i)}" cpuset="{str(i)}"/>
            """
            self.add_item_from_xml(xml_file,"./cputune",temp_info)
        temp_info = f"""
        <emulatorpin cpuset='0'/>
        """
        self.add_item_from_xml(xml_file,"./cputune",temp_info)
        os.system(f"xmllint --format {xml_file} -o {xml_file}")
        pass

    def update_vcpu(self, xml_file, index, value):
        """
        <cputune>
        <vcpupin cpuset="1" vcpu="8" />
        <vcpupin cpuset="2" vcpu="3" />
        <vcpupin cpuset="3" vcpu="4" />
        <emulatorpin cpuset='1'/>
        </cputune>
        """
        tree = ET.parse(xml_file)
        item = tree.find("cputune")
        item[index].set(str("cpuset"), str(value))
        tree.write(xml_file)

    def update_vcpu_emulatorpin(self, xml_file, value):
        """
        <cputune>
        <vcpupin cpuset="1" vcpu="8" />
        <vcpupin cpuset="2" vcpu="3" />
        <vcpupin cpuset="3" vcpu="4" />
        <emulatorpin cpuset='1'/>
        </cputune>
        """
        tree = ET.parse(xml_file)
        item = tree.find("./cputune/emulatorpin")
        item.set(str("cpuset"), str(value))
        tree.write(xml_file)

    def update_numa(self, xml_file, value):
        """
        <numatune>
        <memory mode='strict' nodeset='0'/>
        </numatune>
        """
        tree = ET.parse(xml_file)
        item = tree.find("numatune")
        item[0].set("nodeset", str(value))
        tree.write(xml_file)

    def update_image_source(self, xml_file, image_name):
        """
        <devices>
            <emulator>/usr/libexec/qemu-kvm</emulator>
            <disk device="disk" type="file">
                    <driver name="qemu" type="qcow2" />
                    <source file="/root/rhel.qcow2" />
                    <target bus="virtio" dev="vda" />
                    <address bus="0x01" domain="0x0000" function="0x0" slot="0x00" type="pci" />
            </disk>
        """
        tree = ET.parse(xml_file)
        root = tree.getroot()
        source_item = ET.ElementPath.find(root, "./devices/disk/source")
        source_item.set(str("file"), str(image_name))
        tree.write(xml_file)

    def update_vhostuser_interface(self, xml_file, mac,slot,queue_num):
        """
        <interface type='vhostuser'>
        <mac address='52:54:00:11:8f:ea'/>
        <source type='unix' path='/tmp/vhost0' mode='server'/>
        <model type='virtio'/>
        <driver name='vhost' iommu='on' ats='on' queues='1' rx_queue_size='1024' tx_queue_size='1024'/>
        <address type='pci' domain='0x0000' bus='0x03' slot='0x00' function='0x0'/>
        </interface>
        """
        tree = ET.parse(xml_file)
        item_list = tree.findall("devices/interface")
        for item in item_list:
            if item.get("type") == "vhostuser":
                item[0].set("address", str(mac))
                item[3].set("queues",str(queue_num))
                item[4].set("slot", str(slot))
        tree.write(xml_file)

    def update_bridge_interface(self, xml_file, mac, index=0):
        """
        <interface type='bridge'>
        <mac address='52:54:00:bb:63:7b'/>
        <source bridge='virbr0'/>
        <model type='virtio'/>
        <address type='pci' domain='0x0000' bus='0x02' slot='0x00' function='0x0'/>
        </interface>
        """
        tree = ET.parse(xml_file)
        item_list = tree.findall("./devices/interface[@type='bridge']")
        item = item_list[index]
        if None != item:
            item[0].set("address", str(mac))
        tree.write(xml_file)

    def format_item(self, info, format_list):
        if info:
            return str(info).format(*format_list)

    def add_item_from_xml(self, xml_file, parent_path, xml_info):
        tree = ET.parse(xml_file)
        item = ET.fromstring(xml_info)
        root = tree.getroot()
        parent_item = ET.ElementPath.find(root, parent_path)
        # Here if use if parent_item, this will false when parent_item have no child
        if None != parent_item:
            parent_item.append(item)
        tree.write(xml_file)

    def remove_item_from_xml(self, xml_file, path, index=None):
        tree = ET.parse(xml_file)
        root = tree.getroot()
        parent_path = path + "/.."
        parent_item = ET.ElementPath.find(root, parent_path)
        item_list = ET.ElementPath.findall(root, path)
        if None is index:
            for item in item_list:
                parent_item.remove(item)
        else:
            parent_item.remove(item_list[int(index)])
        tree.write(xml_file)

    def get_pci_address_of_vm_hostdev(self, xml_file, index=0):
        """
        <interface type='hostdev' managed='yes'>
                <mac address='52:54:00:7e:f4:6d'/>
                <driver name='vfio'/>
                <source>
                <address type='pci' domain='0x0000' bus='0x04' slot='0x10' function='0x1'/>
                </source>
                <alias name='hostdev1'/>
                <address type='pci' domain='0x0000' bus='0x04' slot='0x00' function='0x0'/>
        </interface>
        """

        all_hostdev_item = []
        tree = ET.parse(xml_file)
        item_list = tree.findall("devices/interface")
        for item in item_list:
            if item.get("type") == "hostdev":
                all_hostdev_item.append(item)
        if len(all_hostdev_item) > index:
            all_str = ""
            for i in list(all_hostdev_item[index]):
                if i.tag == "address" and i.get("type") == "pci":
                    all_str += str(i.get("domain"))[2:]
                    all_str += ":"
                    all_str += str(i.get("bus"))[2:]
                    all_str += ":"
                    all_str += str(i.get("slot"))[2:]
                    all_str += "."
                    all_str += str(i.get("function"))[2:]
                    break
            return all_str
        else:
            return ""

    def get_mac_address_of_vm_hostdev(self, xml_file, index=0):
        """
        <interface type='hostdev' managed='yes'>
            <mac address='52:54:00:7e:f4:6d'/>
            <driver name='vfio'/>
            <source>
                <address type='pci' domain='0x0000' bus='0x04' slot='0x10' function='0x1'/>
            </source>
        <alias name='hostdev1'/>
        <address type='pci' domain='0x0000' bus='0x04' slot='0x00' function='0x0'/>
        </interface>
        """

        all_hostdev_item = []
        tree = ET.parse(xml_file)
        item_list = tree.findall("devices/interface")
        for item in item_list:
            if item.get("type") == "hostdev":
                all_hostdev_item.append(item)
        if len(all_hostdev_item) > index:
            all_str = ""
            for i in list(all_hostdev_item[index]):
                if i.tag == "mac":
                    all_str = str(i.get("address"))
                    break
            return all_str
        else:
            return ""

    def config_ssh_trust(self,file_name, remote_host, username, password):
        client = paramiko.SSHClient()
        client.load_system_host_keys()
        #client.set_missing_host_key_policy(paramiko.WarningPolicy())
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        with open(file_name, "r") as fd:
            kaizi = fd.read()
        client.connect(remote_host, username=username, password=password)
        client.exec_command("mkdir -p /root/.ssh/")
        client.exec_command(
            'test -f /root/.ssh/known_hosts || touch /root/.ssh/known_hosts')
        cmd = "echo %s >> /root/.ssh/authorized_keys" % (kaizi.strip('\n'))
        client.exec_command(cmd)
        client.close()


    def get_isolate_cpus(self):
        """Here Get all cpu from this system without cpu0"""

        command = "cat /proc/cpuinfo | grep processor | awk '{print $NF}'"
        out = run_and_getout(command)
        str_out = out.decode(self.default_code).replace('\n', ' ').strip()
        str_out = str(str_out)
        if str_out[0] == "0":
                return str_out[2:]
        else:
                return str_out

    def get_isolate_cpus_with_nic(self, nic_name):
        """
            First get cpu numa node and then get cpu list without cpu 0
        """
        cmd = "cat /sys/class/net/{}/device/numa_node".format(str(nic_name))
        out = run_and_getout(cmd)
        cpu_cmd = "lscpu | grep 'NUMA node%s' | awk '{print $NF}'" % (str(int(out)))
        cpu_info = run_and_getout(cpu_cmd)
        str_out = cpu_info.decode(self.default_code).strip()
        #0,1,2,3,4
        #0-9,9-29
        temp_list = str(str_out).split(',')
        all_str = ""
        for i in temp_list:
            if '-' in i:
                start_index = int(str(i).split('-')[0])
                last_index = int(str(i).split('-')[-1])+1
                all_str += " ".join([str(i) for i in range(start_index, last_index)]) + " "
            else:
                all_str += str(i)
                all_str += " "

        all_str = all_str.strip()
        if all_str[0] == "0":
            return all_str[2:]
        else:
            return all_str

    #get numa cpu list without 0 core
    def get_isolate_cpus_on_numa(self, numa):
        cpu_cmd = "lscpu | grep 'NUMA node%s' | awk '{print $NF}'" % (
            str(int(numa)))
        cpu_info = run_and_getout(cpu_cmd)
        str_out = cpu_info.decode(self.default_code).strip()
        # 0,1,2,3,4
        # 0-9,9-29
        temp_list = str(str_out).split(',')
        all_str = ""
        for i in temp_list:
            if '-' in i:
                start_index = int(str(i).split('-')[0])
                last_index = int(str(i).split('-')[-1])+1
                all_str += " ".join([str(i)
                                     for i in range(start_index, last_index)]) + " "
            else:
                all_str += str(i)
                all_str += " "

        all_str = all_str.strip()
        if all_str[0] == "0":
            return all_str[2:]
        else:
            return all_str

    #with hyper thread , get other cores depond on input core
    #As of now ,only one core would be get, since one core only has one sibling
    def get_thread_sibling_core(self,core):
        cmd = f"""
        cat /sys/devices/system/cpu/cpu{core}/topology/thread_siblings_list
        """
        sibling_list = run_and_getout(cmd)
        all_core_list = sibling_list.decode().strip().split(',')
        if str(core) in all_core_list:
            all_core_list.remove(str(core))
        return all_core_list

    #get cpu list with no SMT core
    def get_isolate_cpus_on_numa_no_smt(self, numa):
        str_cpu_all = self.get_isolate_cpus_on_numa(numa)
        cpu_list = str_cpu_all.split()
        cpu_list.sort(key=int)
        copy_cpu_list = cpu_list.copy()
        for i in copy_cpu_list:
            sibling_cores = self.get_thread_sibling_core(i)
            if len(sibling_cores) > 0:
                if int(sibling_cores[0]) < int(i):
                    break
                else:
                    cpu_list.remove(sibling_cores[0])
            else:
                break
        return cpu_list

    def get_pmd_cpu_list_and_mask(self,numa_node,queue_num):
        sibling_core = self.get_thread_sibling_core(0)
        if len(sibling_core) > 0:
            index = queue_num
        else:
            index = queue_num * 2
        all_cpu_list = self.get_isolate_cpus_on_numa_no_smt(numa_node)
        if index  >= len(all_cpu_list):
            return None,0x0
        temp_pmd_list = all_cpu_list[0:index]
        pmd_cpu_list = []
        pmd_cpu_list.extend(temp_pmd_list)
        for pmd in temp_pmd_list:
            sibling_core = self.get_thread_sibling_core(pmd)
            if len(sibling_core) > 0:
                pmd_cpu_list.append(sibling_core[0])
        pmd_cpu_mask = self.get_pmd_masks(" ".join(pmd_cpu_list))
        return pmd_cpu_list,pmd_cpu_mask

    def get_vm_cpu_list(self,numa_node,queue_num):
        sibling_core = self.get_thread_sibling_core(0)
        if len(sibling_core) > 0:
            index = queue_num
        else:
            index = queue_num * 2
        all_cpu_list = self.get_isolate_cpus_on_numa_no_smt(numa_node)
        if index  >= len(all_cpu_list):
            return None
        temp_vm_list = all_cpu_list[0:index]
        vm_cpu_list = []
        vm_cpu_list.extend(temp_vm_list)
        for pmd in temp_vm_list:
            sibling_core = self.get_thread_sibling_core(pmd)
            if len(sibling_core) > 0:
                vm_cpu_list.append(sibling_core[0])
        vm_cpu_list.insert(0,all_cpu_list[index])
        return vm_cpu_list

    def get_pmd_and_vm_cpu_list(self,numa_node,queue_num):
        pmd_cpu_list,pmd_cpu_mask = self.get_pmd_cpu_list_and_mask(numa_node,queue_num)
        #for vm cpu list
        # we select another numa node which is different with pmd cpu
        # to avoid cpu number is not enough
        if numa_node == 0:
            vm_cpu_list = self.get_vm_cpu_list(1,queue_num)
        else:
            vm_cpu_list = self.get_vm_cpu_list(0,queue_num)
        if pmd_cpu_list != None and pmd_cpu_list != 0x0 and vm_cpu_list != None:
            return " ".join(pmd_cpu_list),pmd_cpu_mask," ".join(vm_cpu_list)
        else:
            return None,0x0,None

    def get_pmd_masks(self, str_cpulist):
        ret_val = 0x0
        if str_cpulist == None or str_cpulist == "":
            return 0x0
        else:
            #print(type(str_cpulist))
            if isinstance(str_cpulist, str):
                for i in str_cpulist.split():
                    ret_val |= 0x1 << int(i)
                return hex(ret_val)
            else:
                ret_val |= 0x1 << int(str_cpulist)
                return hex(ret_val)
        pass

    def conf_hugepage(self, item, addvalue):
        pass

    def make_xena_config(self, template_file,module_index):
        if os.path.exists(template_file):
            fd = open(template_file, "r")
            if fd:
                data_json = json.loads(fd.read())
                fd.close()
                data_json['PortHandler']['EntityList'][0]['PortRef']['ModuleIndex'] = module_index
                #data_json['PortHandler']['EntityList'][1]['PortRef']['ModuleIndex'] = module_index
                #here means 100G
                if module_index == 5:
                    data_json['PortHandler']['EntityList'][0]['EnableFec'] = "false"
                    #data_json['PortHandler']['EntityList'][1]['EnableFec'] = "false"
            else:
                print("Can not open %s File "%(template_file))
                return
            with open(template_file,"w") as nfd:
                nfd.write(json.dumps(data_json,indent=4))
        else:
            pass
        pass

    def update_ssh_trust(self,remote_host):
        cmd = f"""
        mkdir -p ~/.ssh
        [[ -f ~/.ssh/known_hosts ]] || touch ~/.ssh/known_hosts
        [[ -f ~/.ssh/config ]] || touch ~/.ssh/config
        chmod 644 ~/.ssh/known_hosts
        chmod 644 ~/.ssh/config
        ssh-keyscan -t rsa {remote_host} >> ~/.ssh/known_hosts
        [[ ! -f ~/.ssh/id_rsa ]] && echo 'y' | ssh-keygen -f ~/.ssh/id_rsa -t rsa -N ''
        """
        run_and_getout(cmd)
        pass

    def get_bos_conn(self,remote_host,user,password):
        import paramiko
        client = paramiko.SSHClient()
        client.load_system_host_keys()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(remote_host, username=user, password=password,allow_agent=False)
        return client

    #url format https://xxx.git
    def git_clone_url(self,url):
        hostname = "netqe-infra01.knqe.lab.eng.bos.redhat.com"
        user = "root"
        password = "100yard-"
        print("*"*80)
        print(f"START remote {hostname} git clone {url}")
        self.update_ssh_trust(hostname)
        client = self.get_bos_conn(hostname,user,password)
        dir_name = os.path.basename(url).split(".")[0]
        import uuid
        uuid_dir = str(uuid.uuid1())
        cmd = f"""
        mkdir -p /home/wanghekai/{uuid_dir}
        pushd /home/wanghekai/{uuid_dir}
        rm -rf {dir_name}
        test -d {dir_name} || git clone {url}
        pushd {dir_name}
        git submodule update --init
        popd
        popd
        """
        r1,r2,r3 = client.exec_command(cmd)
        print(f"END remote {hostname} git clone {url}")
        print("*"*80)
        # print(r1)
        # print(r2.readlines())
        # print(r3.readlines())
        print("*"*80)
        print(f"{url} sync START")
        cmd = f"""
        SSH_AUTH_SOCK= sshpass -p {password} rsync -a {user}@{hostname}:/home/wanghekai/{uuid_dir}/{dir_name} ./
        ls -al
        """
        run_and_getout(cmd)
        print(f"{url} sync END")
        print("*"*80)
        #clean remote host dir
        cmd = f"""
        rm -rf /home/wanghekai/{uuid_dir}
        """
        client.exec_command(cmd)
        #close client
        client.close()
        pass

    def run_cmd_get_output(self, pts, cmd, end_flag="]#"):
        if not os.path.exists(pts):
            return "pts not found"
        sr = serial.Serial(pts, 115200, timeout=0.1)
        if not sr:
            return "open dev pts failed"
        sio = io.TextIOWrapper(io.BufferedRWPair(sr, sr))
        sio.write(os.linesep)
        sio.flush()
        while True:
            data = sio.readline()
            if data == '':
                continue
            else:
                # print(data)
                if "login:" in data and "root" not in data:
                    sio.write("root" + os.linesep)
                    sio.flush()
                elif "Password:" in data:
                    sio.write("redhat" + os.linesep)
                    sio.flush()
                elif "]#" in data or end_flag in data:
                    break
                else:
                    continue
        cmd = cmd + os.linesep
        all_data = ""
        cmds = cmd.split(os.linesep)
        cmds = [i.strip() for i in cmds]
        cmds = [i for i in cmds if len(i) > 0]
        for cmd in cmds:
            while True:
                data = sio.readline()
                if len(data) == 0:
                    sio.write(os.linesep)
                    sio.flush()
                else:
                    # print(data)
                    if "]#" in data or end_flag in data:
                        sio.write(cmd + os.linesep)
                        sio.flush()
                        break
                    else:
                        continue
            while True:
                data = sio.readline()
                if len(data) == 0:
                    sio.write(os.linesep)
                    sio.flush()
                else:
                    # print(data)
                    if "]#" in data or end_flag in data:
                        break
                    else:
                        if len(data.strip(os.linesep)):
                            all_data = all_data + data

        return all_data


if __name__ == '__main__':
    import fire
    fire.Fire(Tools)