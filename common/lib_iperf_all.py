#!/usr/bin/env python3

from beaker_cmd import log
from beaker_cmd import run
from beaker_cmd import raw_string_print
from bash import bash
from tools import Tools
my_tool = Tools()
def vm_run_cmd(vm_name,cmd,log_flag=True):
    pts = bash(f"virsh ttyconsole {vm_name}").value()
    ret = my_tool.run_cmd_get_output(pts, cmd)
    if log_flag == True:
        raw_string_print(ret)
    pass

def do_host_iperf(ipv4_addr,ipv6_addr):
    log(f"ipv4 addr : {ipv4_addr}")
    log(f"ipv6 addr : {ipv6_addr}")
    cmd = f"""
    ping {ipv4_addr} -c 10
    ping6 {ipv6_addr} -c 10
    """
    run(cmd)

    cmd = f"""
    iperf3 -c {ipv4_addr} -p 8080
    iperf3 -c {ipv6_addr} -p 8080
    iperf3 -c {ipv4_addr} -p 8080 -u -b0
    iperf3 -c {ipv6_addr} -p 8080 -u -b0
    """
    run(cmd)
    pass

def do_vm_iperf(vm_name,ipv4_addr,ipv6_addr):
    log(f"vm name : {vm_name}")
    log(f"ipv4 addr : {ipv4_addr}")
    log(f"ipv6 addr : {ipv6_addr}")
    cmd = f"""
    ping {ipv4_addr} -c 10
    ping6 {ipv6_addr} -c 10
    iperf3 -c {ipv4_addr} -p 8080
    iperf3 -c {ipv6_addr} -p 8080
    iperf3 -c {ipv4_addr} -p 8080 -u -b0
    iperf3 -c {ipv6_addr} -p 8080 -u -b0
    """
    vm_run_cmd(vm_name,cmd)
    pass


def do_ns_netperf(ns,ipv4_addr,ipv6_addr):
    log(f"namesapce {ns}")
    log(f"ipv4 addr : {ipv4_addr}")
    log(f"ipv6 addr : {ipv6_addr}")
    cmd = f"""
    ip netns exec {ns} ping {ipv4_addr} -c 10
    ip netns exec {ns} ping6 {ipv6_addr} -c 10
    """
    run(cmd)

    cmd = f"""
    ip netns exec {ns} iperf3 -c {ipv4_addr} -p 8080
    ip netns exec {ns} iperf3 -c {ipv6_addr} -p 8080
    ip netns exec {ns} iperf3 -c {ipv4_addr} -p 8080 -u -b0
    ip netns exec {ns} iperf3 -c {ipv6_addr} -p 8080 -u -b0
    """
    run(cmd)
    pass
